import { Auth } from "aws-amplify";
import { all, call, put, takeLatest } from "redux-saga/effects";
import userInformationActions, {
    getUserBasicInformationRequest,
    getUserBasicInformationSuccess,
    getUserBasicInformationFailure,
    saveUserBasicInformationRequest,
    saveUserBasicInformationSuccess,
    saveUserBasicInformationFailure,
} from "../reducers/Actions/userInformationActions";

export function* getUserBasicInformation({ payload: { symptomText } }) {
    try {
      debugger;
      const { result, message } = yield call(getUserBasicInformationWithAPI, symptomText);
  
      if (result) {
        console.log(result);
        yield put(getUserBasicInformationSuccess(result));
      } else {
        yield put(getUserBasicInformationFailure(message));
      }
    } catch (message) {
      console.log(message);
      yield put(getUserBasicInformationFailure(message));
    }
  }


  
  export function* saveUserBasicInformation({ payload: { userInformation } }) {
    try {
      debugger;
      const { result, message } = yield call(saveUserBasicInformationWithAPI, userInformation);
  
      if (result) {
        console.log(result);
        yield put(saveUserBasicInformationSuccess(result));
      } else {
        yield put(saveUserBasicInformationFailure(message));
      }
    } catch (message) {
      console.log(message);
      yield put(saveUserBasicInformationFailure(message));
    }
  }

  async function saveUserBasicInformationWithAPI(
    symptomSeverity: { payload: any; _id: any },
    _id: any
  ) {
    try {
      const symptomSeverityInput = {
        symptoms: symptomSeverity.payload,
        userId: symptomSeverity._id,
      };
      const init = {
        headers: {
          Authorization: `${(await Auth.currentSession())
            .getIdToken()
            .getJwtToken()}`,
          "Content-Type": "application/json",
        },
        method: "post",
        body: JSON.stringify(symptomSeverityInput),
      };
      console.log("Calling API");
      let response = await fetch(`${process.env.REACT_APP_URL}/case/`, init);
      response = await response.json();
      return response;
    } catch (err) {
      console.log(err.stack);
      console.log("error with Get API");
    }
  }  


export function* userInformationSagas() {
    yield all([
      yield takeLatest(userInformationActions.GET_USER_BASIC_INFORMATION_REQUEST, getUserBasicInformation),
      yield takeLatest(
        userInformationActions.SAVE_USER_BASIC_INFORMATION_REQUEST,
        saveUserBasicInformation
      ),
    ]);
  }