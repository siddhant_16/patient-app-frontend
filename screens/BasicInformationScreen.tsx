import { H2, Text, Textarea } from "native-base";
import React, { memo, useState } from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import classes from "../assets/css/RegisterLoginViewStyle";
import WaitMessage from "../components/WaitMessage";
import DatePicker from "react-native-datepicker";

import { saveUserBasicInformationRequest } from "../reducers/Actions/userInformationActions";
import {
    Container,
    Header,
    Content,
    Item,
    Input,
    Label,
    Form,
    Icon,
    Button,
    H3,
  } from "native-base";
  import { ScrollView } from "react-native-gesture-handler";

const BasicInformationView = ({saveUserBasicInformationRequest, auth, navigation}) => {
    const [gender, setGender] = useState("");
    const [dateOfBirth, setDateOfBirth] = useState("");
    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    const handleSubmit = () => {
        saveUserBasicInformationRequest({
            gender,
            dateOfBirth,
            height,
            weight,
        });
    };

    const {savingUserBasicData, userBasicDataSaved, userBasicDataSaveError, userBasicData, userBasicDataRetrievedError, 
        gettingUserBasicData, userBasicDataRetrieved } = auth;
    
    

    return (
        <ScrollView style={classes.container}>
            <Form style={styles.loginForm}>
                <Item style={styles.labelheader}><H3>It is important to input your information and keep it up to date</H3></Item>
                <DatePicker round style={styles.input}
                //style={styles.datePickerStyle}
                date={dateOfBirth} // Initial date from state
                mode="date" // The enum of date, datetime and time
                placeholder="Date of Birth"
                format="DD-MM-YYYY"
                minDate="01-01-1930"
                maxDate="01-01-2019"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                    //display: 'none',
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                    },
                    dateInput: {
                    marginLeft: 36,
                    borderRadius: 15,
                    },
                }}
                onDateChange={(date) => {
                    setDateOfBirth(date);
                }}
                />
            </Form>
        </ScrollView>
    )
};


const styles = StyleSheet.create({
    boldText: {
      fontWeight: "bold",
    },
    
    labelheader: {
        paddingVertical: 20,
    },

    input: {
      marginVertical: 30,
      marginHorizontal: 0,
      alignSelf: "center",
      borderRadius: 15,
      minWidth: 300,
    },

    label: {
      paddingLeft: 25,
    },

    loginForm: {
      flex: 1,
      flexDirection: "column",
      justifyContent: "flex-start",
      alignItems: "center",
    },

    datePickerStyle: {
        width: 200,
        marginTop: 20,
      },
  });
  
const mapDispatchToProps = (dispatch: (arg0: { type: string; }) => any) => ({
    saveUserBasicInformationRequest: ({ gender, dateOfBirth, height, weight }) =>
      dispatch(saveUserBasicInformationRequest({gender, dateOfBirth, height, weight})),
  });
  
const mapStateToProps = ({ auth }) => ({ auth });
  
export default connect(mapStateToProps, mapDispatchToProps)(BasicInformationView);
  