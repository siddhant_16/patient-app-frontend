import { Button, H2, Icon, Text, Textarea } from "native-base";
import React, { memo, useState } from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import classes from "../assets/css/RegisterLoginViewStyle";
import WaitMessage from "../components/WaitMessage";
import { getSymptomsRequest } from "../reducers/Actions/symptomsActions";

const CaseCreationScreen = memo(({ getSymptomsRequest, auth, navigation }) => {
  const [confirmed, setConfirmed] = useState(false);
  const [symptomText, setSymptomsText] = useState("");
  const [details] = useState("");
  const [uploads] = useState("");
  const handleSubmit = () => {
    getSymptomsRequest({
      symptomText
    });
    //setConfirmed(true);
    // setTimeout(() => {
    //   //   navigation.push();
    // }, 5000);
  };

  const { retrievingSymptoms, symptomsRetrieved, retrievingSymptomsError, fullUserDetail, symptomList } = auth

  const validate = () => 
    symptomText.length > 0;

  // const successfulSymptomRetrieved =  (!retrievingSymptoms && symptomsRetrieved && symptomList.length != 0)
  //   ? navigation.navigate("ForgotPassword") : null;

  return (
    <>
      {!retrievingSymptoms && (
        <View style={[styles.root, { marginHorizontal: 20 }]}>
          <H2 style={styles.textBottomBorder}>Symptoms</H2>
          {/* <View>{retrievingSymptomsError}</View> */}
          <Textarea
            rowSpan={8}
            bordered
            placeholder="No Symptoms added, Please add"
            style={styles.border}
            onChangeText={(e) => setSymptomsText(e)}
            value={symptomText}
          />

          <Button transparent iconLeft>
            <Icon name="plus" type="FontAwesome5" style={styles.icon} />
            <Text>Add Uploads</Text>
          </Button>

          <Button rounded style={classes.button} onPress={() => handleSubmit()} disabled={!validate()}>
            <H2 style={classes.buttonText}>Confirm</H2>
            <Icon
              name="chevron-right"
              type="FontAwesome5"
              style={[classes.buttonText, { marginLeft: 10 }]}
            />
          </Button>
        </View>
      )}
      {retrievingSymptoms && (
        <WaitMessage
          text={[
            "Thank You.",
            "Please wait while your case is being accessed.",
          ]}
        />
      )}
    </>
  );
});

const styles = StyleSheet.create({
  icon: { color: "#2a9fd8" },
  root: {
    flex: 1,
    flexDirection: "column",
    height: "100%",
    backgroundColor: "#F4F8F9",
  },
  textBottomBorder: {
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginTop: 30,
  },
  border: {
    marginVertical: 20,
    justifyContent: "flex-start",
    borderRadius: 10,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "grey",
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
  },
});

const mapStateToProps = ({ auth }) => ({ auth });

const mapDispatchToProps = (dispatch: (arg0: { type: string; payload: { symptomText: any; }; }) => any) => ({
  getSymptomsRequest: ({ symptomText }) =>
    dispatch(getSymptomsRequest({ symptomText })),
});

export default connect(mapStateToProps, mapDispatchToProps)(CaseCreationScreen);
