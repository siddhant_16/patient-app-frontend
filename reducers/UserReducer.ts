import * as errors from "./Actions/errors";

import AuthActions from "./Actions/authActions";
import UserInformationActions from "./Actions/userInformationActions";

const initialState = {
  currentUser: null,
  error: "",
  caseConfirmDetailsAccessed: false,
};

const combinedReducer = (
  state = initialState,
  action: {
    type: any;
    payload: { email: any; user: { username: any }; eventValue: any };
    error: { id: any };
  }
) => {
  switch (action.type) {
    case UserInformationActions.GET_USER_BASIC_INFORMATION_REQUEST:
      return {
        ...state,
        userBasicData: null,
        gettingUserBasicData: true,
        userBasicDataRetrieved: false,
        userBasicDataRetrievedError: null,
      };
    
    case UserInformationActions.GET_USER_BASIC_INFORMATION_SUCCESS:
      return {
        ...state,
        userBasicData: action.payload,
        gettingUserBasicData: false,
        userBasicDataRetrieved: true,
        userBasicDataRetrievedError: null,
      }
    
    case UserInformationActions.GET_USER_BASIC_INFORMATION_FAILURE:
      return {
        ...state,
        userBasicData: null,
        userBasicDataRetrievedError: action.payload,
        gettingUserBasicData: false,
        userBasicDataRetrieved: false,
      }
      
    case UserInformationActions.SAVE_USER_BASIC_INFORMATION_REQUEST:
      return {
          ...state,
          savingUserBasicData: true,
          userBasicDataSaved: false,
          userBasicDataSaveError: null,
      };
      
    case UserInformationActions.SAVE_USER_BASIC_INFORMATION_SUCCESS:
      return {
          ...state,
          savingUserBasicData: false,
          userBasicDataSaved: true,
          userBasicDataSaveError: null,
      }
      
    case UserInformationActions.SAVE_USER_BASIC_INFORMATION_FAILURE:
      return {
        ...state,
        savingUserBasicData: false,
        userBasicDataSaved: false,
        userBasicDataSaveError: action.payload,
      }

    default:
      return state;
  }
};

export default combinedReducer;
