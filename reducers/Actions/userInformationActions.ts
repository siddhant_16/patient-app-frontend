const UserInformationActions = {
    GET_USER_BASIC_INFORMATION_REQUEST: "GET_SYMPTOMS_REQUEST",
    GET_USER_BASIC_INFORMATION_SUCCESS: "GET_USER_BASIC_INFORMATION_SUCCESS",
    GET_USER_BASIC_INFORMATION_FAILURE: "GET_USER_BASIC_INFORMATION_FAILURE",
    SAVE_USER_BASIC_INFORMATION_REQUEST: "SAVE_SYMPTOMS_REQUEST",
    SAVE_USER_BASIC_INFORMATION_SUCCESS: "SAVE_USER_BASIC_INFORMATION_SUCCESS",
    SAVE_USER_BASIC_INFORMATION_FAILURE: "SAVE_USER_BASIC_INFORMATION_FAILURE",
};

export const getUserBasicInformationRequest = () => ({
    type: UserInformationActions.GET_USER_BASIC_INFORMATION_REQUEST
});

export const getUserBasicInformationSuccess = () => ({
    type: UserInformationActions.GET_USER_BASIC_INFORMATION_SUCCESS
});

export const getUserBasicInformationFailure = () => ({
    type: UserInformationActions.GET_USER_BASIC_INFORMATION_FAILURE
});

export const saveUserBasicInformationRequest = ({gender, dateOfBirth, height, weight}) => ({
    type: UserInformationActions.SAVE_USER_BASIC_INFORMATION_REQUEST,
    payload: {gender, dateOfBirth, height, weight}
});

export const saveUserBasicInformationSuccess = () => ({
    type: UserInformationActions.SAVE_USER_BASIC_INFORMATION_SUCCESS
});

export const saveUserBasicInformationFailure = () => ({
    type: UserInformationActions.SAVE_USER_BASIC_INFORMATION_FAILURE
});

export default UserInformationActions;
